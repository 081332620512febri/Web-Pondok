<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pondok-pesantren-bahrul-ulum
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="wp-content/themes/pondok-pesantren/style2.css">
	<!-- bootstrap css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
    crossorigin="anonymous">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'pondok-pesantren' ); ?></a>

		<section class="header" id="header">
        	<nav class="navbar navbar-expand-lg navbar-light bg-light">
            	<div class="container">
					<p class="mt-4">
						<span style="float: left;" class="mr-2" ><?php the_custom_logo(); ?></span>
						<span class="title">
							<?php
							if ( is_front_page() && is_home() ) :
								?>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
								<?php
							else :
								?>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
								<?php
							endif;
							?>
						</span>
						<br>
						<span class="desc">
							<?php
							$pondok_pesantren_description = get_bloginfo( 'description', 'display' );
							if ( $pondok_pesantren_description || is_customize_preview() ) :
								?>
								<?php echo $pondok_pesantren_description; /* WPCS: xss ok. */ ?>
								<?php endif; ?>
						</span>
					</p>
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'pondok-pesantren' ); ?></button>
					<div class="collapse navbar-collapse" id="navbarNav">
                    	<ul class="navbar-nav ml-auto">
						<nav id="site-navigation" class="main-navigation">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							) );
							?>
						</nav>
						</ul>
				</div>
			</nav>
		</section>
		<!-- </div>.site-branding -->

		
			
		</nav><!-- #site-navigation -->
	<!-- </header>#masthead -->

	<div id="content" class="site-content">
