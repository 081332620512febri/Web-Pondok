<?php
/**
 * pondok-pesantren-bahrul-ulum functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pondok-pesantren-bahrul-ulum
 */

if ( ! function_exists( 'pondok_pesantren_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function pondok_pesantren_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on pondok-pesantren-bahrul-ulum, use a find and replace
		 * to change 'pondok-pesantren' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'pondok-pesantren', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'pondok-pesantren' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'pondok_pesantren_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'pondok_pesantren_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pondok_pesantren_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'pondok_pesantren_content_width', 640 );
}
add_action( 'after_setup_theme', 'pondok_pesantren_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pondok_pesantren_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pondok-pesantren' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'pondok-pesantren' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pondok_pesantren_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function pondok_pesantren_scripts() {
	wp_enqueue_style( 'pondok-pesantren-style', get_stylesheet_uri() );

	wp_enqueue_script( 'pondok-pesantren-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'pondok-pesantren-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'pondok_pesantren_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// ------------------------------WIDGET CUSTOM------------------------------ //

function custom_register_widget(){
	register_widget( 'jumbotron');
	register_widget( 'jumbotron_item');
	register_widget( 'jumbotron_end');
}
add_action( 'widgets_init', 'custom_register_widget');

class jumbotron extends Wp_Widget{
	function __construct(){
		parent::__construct(
			'jumbotron',
			__('Widget Jumbotron', 'jumbotron_domain'),
			array(
				'description' => __('Description of widget jumbotron', 'jumbotron_domain')
			)
		);
	}

	function widget($args, $instance){
		?>
		<div class="container">
        <section class="jumbotron mt-5" style="background-image: url('<?=$instance['image_uri']?>');">
            <div class="jumbotron-item">
                <button class="btn btn-success"><?=$instance['button']?></button>
                <h2><?=$instance['title']?></h2>
                <small class="date mr-5"><i class="fas fa-calendar-alt mr-2"></i><?php echo get_the_date()?></small> 
                <small class="user"><i class="fas fa-user mr-2"></i><?=$instance['by']?></small>
                <div class="border-bottom mt-5 mb-5"></div>
            </div>
			<div class="slider" style="padding-left:18px; padding-right:18x;">
			<div class="row">
		<?php
	}

	function form($instance){
		?>
			<p>
				<label for="<?php echo $this->get_field_id('image_uri'); ?>"><?php _e('Image:'); ?></label>
				<div class="widg-img" >
					<img style="width: 100%;" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_image custom_media_image" src="<?php if( !empty( $instance['image_uri'] ) ){echo $instance['image_uri'];} ?>" />
					<input input type="hidden" type="text" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_id custom_media_id" name="<?php echo $this->get_field_name( 'image_id' ); ?>" id="<?php echo $this->get_field_id( 'image_id' ); ?>" value="<?php echo $instance['image_id']; ?>" />
					<input type="text" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_url custom_media_url" name="<?php echo $this->get_field_name( 'image_uri' ); ?>" id="<?php echo $this->get_field_id( 'image_uri' ); ?>" value="<?php echo $instance['image_uri']; ?>" >
					<input type="button" value="Upload Image" class="button custom_media_upload" id="<?php echo $this->get_field_id( 'image_id' ); ?>"/>
				</div>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('button'); ?>"><?php _e('Button :'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('button'); ?>" name="<?php echo $this->get_field_name('button'); ?>" type="text" value="<?php echo esc_attr($instance['button']); ?>" required/>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title :'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" required/>
			</p>
			<p>
			<label for="<?php echo $this->get_field_id('by'); ?>"><?php _e('by :'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('by'); ?>" name="<?php echo $this->get_field_name('by'); ?>" type="text" value="<?php echo esc_attr($instance['by']); ?>" required/>
			</p>

			<script type="text/javascript">
			jQuery(document ).ready( function(){
				function media_upload( button_class ) {
					var _custom_media = true,
					_orig_send_attachment = wp.media.editor.send.attachment;
					jQuery('body').on('click','.custom_media_upload',function(e) {
						var button_id ='#'+jQuery(this).attr( 'id' );
						var button_id_s = jQuery(this).attr( 'id' );
						var self = jQuery(button_id);
						var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = jQuery(button_id);
						var id = button.attr( 'id' ).replace( '_button', '' );
						_custom_media = true;

						wp.media.editor.send.attachment = function(props, attachment ){
							if ( _custom_media ) {
								jQuery( '.' + button_id_s + '_media_id' ).val(attachment.id); 
								jQuery( '.' + button_id_s + '_media_url' ).val(attachment.url);
								jQuery( '.' + button_id_s + '_media_image' ).attr( 'src',attachment.url).css( 'display','block' ); 
								jQuery('#' + id.replace('image_id', 'savewidget')).val('<?php _e('Save'); ?>').prop("disabled", false);
							} else {
								return _orig_send_attachment.apply( button_id, [props, attachment] );
							}
						}
						wp.media.editor.open(button);
						return false;
					});
				}
				media_upload( '.custom_media_upload' );

			});
		</script>
		<?php
	}

	function update($new_instance, $old_instance){
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		$instance['button'] = (!empty($new_instance['button'])) ? strip_tags($new_instance['button']) : '';
		$instance['by'] = (!empty($new_instance['by'])) ? strip_tags($new_instance['by']) : '';
		$instance['image_uri'] = (!empty($new_instance['image_uri'])) ? $new_instance['image_uri'] : '';
		$instance['image_id'] = (!empty($new_instance['image_id'])) ? $new_instance['image_id'] : '';
		return $instance;
	}
}

class jumbotron_item extends Wp_Widget{
	function __construct(){
		parent::__construct(
			'jumbotron_item',
			__('Jumbotron Item', 'jumbotron_item_domain'),
			array(
				'description' => __('Description of jumbotron item', 'jumbotron_item_domain'),
			)
			);
	}

	function widget($args, $instance){
		?>
			<div class="overlay-slide" style="padding: 5px;">
                <img src="<?=$instance['image_uri']?>" alt="">
                <div class="desc">
                    <small class="text-muted mt-0">Ahad, 21-01-2020</small>
                    <h5 class="card-title"><?=$instance['title']?></h5>
                </div>
            </div>
		<?php
	}

	function form($instance){
		?>
			<p>
				<label for="<?php echo $this->get_field_id('image_uri'); ?>"><?php _e('Image:'); ?></label>
				<div class="widg-img" >
					<img style="width: 100%;" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_image custom_media_image" src="<?php if( !empty( $instance['image_uri'] ) ){echo $instance['image_uri'];} ?>" />
					<input input type="hidden" type="text" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_id custom_media_id" name="<?php echo $this->get_field_name( 'image_id' ); ?>" id="<?php echo $this->get_field_id( 'image_id' ); ?>" value="<?php echo $instance['image_id']; ?>" />
					<input type="text" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_url custom_media_url" name="<?php echo $this->get_field_name( 'image_uri' ); ?>" id="<?php echo $this->get_field_id( 'image_uri' ); ?>" value="<?php echo $instance['image_uri']; ?>" >
					<input type="button" value="Upload Image" class="button custom_media_upload" id="<?php echo $this->get_field_id( 'image_id' ); ?>"/>
				</div>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title :'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" required/>
			</p>
			<script type="text/javascript">
			jQuery(document ).ready( function(){
				function media_upload( button_class ) {
					var _custom_media = true,
					_orig_send_attachment = wp.media.editor.send.attachment;
					jQuery('body').on('click','.custom_media_upload',function(e) {
						var button_id ='#'+jQuery(this).attr( 'id' );
						var button_id_s = jQuery(this).attr( 'id' );
						var self = jQuery(button_id);
						var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = jQuery(button_id);
						var id = button.attr( 'id' ).replace( '_button', '' );
						_custom_media = true;

						wp.media.editor.send.attachment = function(props, attachment ){
							if ( _custom_media ) {
								jQuery( '.' + button_id_s + '_media_id' ).val(attachment.id); 
								jQuery( '.' + button_id_s + '_media_url' ).val(attachment.url);
								jQuery( '.' + button_id_s + '_media_image' ).attr( 'src',attachment.url).css( 'display','block' ); 
								jQuery('#' + id.replace('image_id', 'savewidget')).val('<?php _e('Save'); ?>').prop("disabled", false);
							} else {
								return _orig_send_attachment.apply( button_id, [props, attachment] );
							}
						}
						wp.media.editor.open(button);
						return false;
					});
				}
				media_upload( '.custom_media_upload' );

			});
			</script>
		<?php
	}

	function update($new_instance, $old_instance){
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		$instance['image_uri'] = (!empty($new_instance['image_uri'])) ? $new_instance['image_uri'] : '';
		$instance['image_id'] = (!empty($new_instance['image_id'])) ? $new_instance['image_id'] : '';
		return $instance;
	}
}

class jumbotron_end extends Wp_Widget{
	function __construct(){
		parent::__construct(
			'jumbotron_end',
			__('End of Jumbotron', 'jumbotron_end_domain'),
			array(
				'description' => __('Description of jumbotron end', 'jumbotron_end_domain')
			)
		);
	}

	function widget($args, $instance){
		?>
				</div>
				</div>
			</section>
			<div>
		<?php
	}

	function form($instance){

	}

	function update($new_instance, $old_instance){

	}
}